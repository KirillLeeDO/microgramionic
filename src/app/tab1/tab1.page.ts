import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { finalize, catchError } from 'rxjs/operators';
import { of as ObservableOf, Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ShellFeedListModel } from '../shell/shell-feed.model';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  private isFetchingData: boolean;

  private data: Observable<any[]>;
  public posts: Observable<any[]>;
  routeResolveData: ShellFeedListModel;

  constructor(
    public postService: PostService,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private route: ActivatedRoute
  ) { }

  async presentLoader() {
    const loading = await this.loadingCtrl.create({
      spinner: 'dots',
      message: 'Fetching...'
    });

    await loading.present();

    return loading;
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message,
      color: 'danger',
      duration: 2000
    });

    await toast.present();
  }

  async ngOnInit() {
    // console.log(this.route.snapshot.data['data']);
    // const loading = await this.presentLoader();

    // this.data = this.route.snapshot.data['data'].source;

    // this.data.subscribe(() => {
    //   loading.dismiss();
    // })

    this.postService.getPosts().subscribe(test => console.log(test));


    if (this.route && this.route.data) {
      // We resolved a promise for the data Observable
      const promiseObservable = this.route.data;
      console.log('Progressive Shell Resovlers - Route Resolve Observable => promiseObservable: ', promiseObservable);

      if (promiseObservable) {
        promiseObservable.subscribe(promiseValue => {
          
          const dataObservable = promiseValue['data'];
          console.log('Progressive Shell Resovlers - Subscribe to promiseObservable => dataObservable: ', promiseValue, dataObservable);

          if (dataObservable) {
            dataObservable.subscribe(observableValue => {
              const pageData: ShellFeedListModel = observableValue;
              // tslint:disable-next-line:max-line-length
              console.log('Progressive Shell Resovlers - Subscribe to dataObservable (can emmit multiple values) => PageData (' + ((pageData && pageData.isShell) ? 'SHELL' : 'REAL') + '): ', pageData);
              // As we are implementing an App Shell architecture, pageData will be firstly an empty shell model,
              // and the real remote data once it gets fetched
              if (pageData) {
                this.routeResolveData = pageData;
              }
            });
          } else {
            console.warn('No dataObservable coming from Route Resolver promiseObservable');
          }
        });
      } else {
        console.warn('No promiseObservable coming from Route Resolver data');
      }
    } else {
      console.warn('No data coming from Route Resolver');
    }



    // this.posts = this.route.snapshot.data['contact'];
    // const loading = await this.presentLoader();
    // this.isFetchingData = true;
    // this.postService.getPosts()
    //   .pipe(
    //     catchError((err) => {
    //       this.presentToast(err.message);
    //       return ObservableOf(err);
    //     })
    //   )
    //   .subscribe(() => {
    //     loading.dismiss();
    //   })
  }
}
