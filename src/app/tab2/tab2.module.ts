import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';

import { Tab2Page } from './tab2.page';
import { NewPostFormComponent } from '../components/new-post-form/new-post-form.component';
import { LocationPickerComponent } from '../components/location-picker/location-picker.component';
import AppConfig from '../app.config';
import { environment } from 'src/environments/environment';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: Tab2Page }]),
    AgmCoreModule.forRoot({
      // apiKey: AppConfig.GOOGLE_MAPS_KEY_OPEN
      apiKey: environment.gmaps.apiKey
    })
  ],
  declarations: [
    Tab2Page,
    NewPostFormComponent,
    LocationPickerComponent
  ],
  entryComponents: [
    LocationPickerComponent
  ]
})
export class Tab2PageModule {}
