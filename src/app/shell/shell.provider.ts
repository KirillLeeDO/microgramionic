import { Observable, BehaviorSubject, forkJoin, of } from 'rxjs';
import { first, delay, finalize } from 'rxjs/operators';

export class ShellProvider<T> {
  private _observable: Observable<T>;

  // A Subject that requires an initial value and emits its current value to new subscribers
  // If we choose a BehaviorSubject, new subscribers will only get the latest value (real data).
  // This is useful for repeated use of the resolved data (navigate to a page, go back, navigate to the same page again)
  private _subject: BehaviorSubject<T>;

  // We wait on purpose 2 secs on local environment when fetching from json to simulate the backend roundtrip.
  // However, in production you should set this delay to 0 in the environment.ts file.
  private networkDelay = 2000;

  // To debug shell styles, change configuration in the environment.ts file
  private debugMode = false;

  constructor(shellModel: T, dataObservable: Observable<T>) {
    const shellClassName = (shellModel && shellModel.constructor && shellModel.constructor.name) ? shellModel.constructor.name : 'No Class Name';
    console.time('[' + shellClassName + '] ShellProvider roundtrip');
    
    // Set the shell model as the initial value
    this._subject = new BehaviorSubject<T>(shellModel);

    dataObservable.pipe(
      first()
    )
    .subscribe((dataValue: T) => {
      if (!this.debugMode) {
        this._subject.next(dataValue);
        console.timeEnd('[' + shellClassName + '] ShellProvider roundtrip');
      }
    });

    this._observable = this._subject.asObservable();
  }

  public get observable(): Observable<T> {
    return this._observable;
  }
}