import Location from '../models/Location';

export class ShellFeedModel {
  data: string;
  location: Location;
  description: string;
  postedOn: any;
  key?: string;
}

export class ShellFeedListModel {
  items: ShellFeedModel[] = [
    new ShellFeedModel(),
    new ShellFeedModel(),
    new ShellFeedModel()
  ];

  constructor(readonly isShell: boolean) {
    
  }
}