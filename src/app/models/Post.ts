import uuid from 'uuid/v1';

class Post {
  data: any;
  location: any;
  description: string;
  postedOn: any;
  key?: string;

  constructor(data: any = null, location: any = null, description: string = null) {
    this.data = data;
    this.location = location;
    this.description = description;
  }

  generateId() {
    return uuid();
  }
}

export default Post;