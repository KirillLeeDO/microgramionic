import Place from './Place';

class Location {
  latitude: number;
  longitude: number;
  place: Place;

  constructor(latitude: number = null, longitude: number = null, place: Place = null) {
    this.latitude = latitude;
    this.longitude = longitude;
    this.place = place;
  }
}

export default Location;