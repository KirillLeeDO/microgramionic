class Place {
  country: string;
  locality: string;

  address: string;

  constructor(country: string, locality: string) {
    this.country = country;
    this.locality = locality;

    this.address = this.buildAddress();
  }

  private buildAddress(): string {
    let address = this.locality;

    if (this.country.length) {
      address += `, ${this.country}`;
    }

    return address;
  }

  getAddress(): string {
    return this.address;
  }
}

export default Place;