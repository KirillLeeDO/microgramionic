class UserCredentials {
  constructor(public email: string, public password: string) { }
}

export default UserCredentials;