import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { ActionSheetController, NavController, LoadingController, ModalController, AlertController, ToastController, Platform } from '@ionic/angular';

import { PostService } from '../../services/post.service';
import { GeolocationService } from '../../services/geolocation.service';
import Post from '../../models/Post';
import Place from '../../models/Place';
import Location from '../../models/Location';
import { LocationPickerComponent } from '../location-picker/location-picker.component';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-new-post-form',
  templateUrl: './new-post-form.component.html',
  styleUrls: ['./new-post-form.component.scss'],
})
export class NewPostFormComponent implements OnInit {
  defaultLocation: Location = new Location();
  currentLocation: Location = new Location();
  currentPost: Post = new Post();

  inputImageBlob: any = null;
  imageSrcData: any = null;

  private isDesktop: boolean = false;

  @ViewChild('fileInput') fileInputRef: ElementRef;

  private loadingElement: HTMLIonLoadingElement

  constructor(
    public actionSheetController: ActionSheetController,
    public postService: PostService,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private geoService: GeolocationService,
    public webView: WebView,
    private ngZone: NgZone,
    private platform: Platform
  ) { }

  ngOnInit() {
    // @TODO: Extract this to PLatformService
    this.platform.ready()
      .then((source) => {
        if (
          this.platform.is('desktop') ||
          this.platform.is('mobileweb')
        ) {
          this.isDesktop = true;
        }

      });

    this.geoService.getCurrentLocation()
      .then((location: Location) => {
        if (location === null) {
          this.currentLocation = this.defaultLocation = null;
          return;
        }
        
        this.currentLocation = location;
        this.defaultLocation = cloneDeep(this.currentLocation);
      });
  }

  async presentLoader() {
    this.loadingElement = await this.loadingCtrl.create({
      spinner: 'dots',
      message: 'Posting...'
    });

    await this.loadingElement.present();
  }

  async presentAlert(message) {
    const alert = await this.alertCtrl.create({
      header: 'Oops!',
      message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message,
      color: 'danger',
      duration: 2000
    });

    await toast.present();
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: LocationPickerComponent,
      componentProps: {
        location: this.currentLocation
      }
    });

    modal.onDidDismiss()
      .then(({ data }) => {
        console.log('look data from modal', data);
        if (!data) {
          return;
        }

        this.currentLocation = data;
      });

    return await modal.present();
  }

  loadPhotoFromInput(files: FileList) {
    if (!files.length) {
      return;
    }

    const chosenFile = files[0];
    const fileName: any = chosenFile.name;

    this.inputImageBlob = {
      fileName,
      imgBlob: chosenFile
    };

    const reader: FileReader = new FileReader();
    reader.readAsDataURL(chosenFile)
    reader.onloadend = () => {
      this.imageSrcData = reader.result;
    }
  }

  async addPhoto() {
    if (this.isDesktop) {
      this.fileInputRef.nativeElement.click();
      return;
    }

    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Photo',
          icon: 'camera',
          handler: () => {
            this.postService.takePicture()
              .then((post: Post) => {
                this.currentPost.data = post.data;
                this.imageSrcData = this.webView.convertFileSrc(post.data);
              })
              .catch((error) => {
                console.log('camera issue', error);
              })
          }
        },
        {
          text: 'Gallery',
          icon: 'folder',
          handler: () => {
            this.postService.openPicture()
              .then((posts: Post[]) => {
                this.ngZone.run(() => {
                  this.currentPost.data = posts[0].data;
                  this.imageSrcData = this.webView.convertFileSrc(posts[0].data); 
                });
              })
              .catch((error) => {
                console.log('picker issue', error);
              })
          }
        }
      ]
    });

    await actionSheet.present();
  }

  async submitPost($event) {
    const { data, description } = this.currentPost;
    $event.preventDefault();

    let errors = [];
    if (description === null) {
      errors.push('Description is empty.');
    }

    if (data === null && this.inputImageBlob === null) {
      errors.push('Pick an image.');
    }

    if (errors.length) {
      this.presentAlert(errors.join('<br>'));
      return;
    }
    
    await this.presentLoader();

    this.currentPost.location = this.currentLocation;

    try {
      await this.postService.addPost(this.currentPost, this.inputImageBlob);
      this.navCtrl.navigateRoot('tabs/feed');
    } catch (e) {
      this.presentToast(e);
      console.log(e);
    } finally {
      this.loadingElement.dismiss();
      this.currentPost = new Post();
    }
  }

  pickLocation() {
    this.presentModal();
  }

  resetLocation() {
    const defaultPlace = this.defaultLocation.place;
    this.currentLocation = cloneDeep(this.defaultLocation);
  }
}
