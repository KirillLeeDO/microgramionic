import { Component, OnInit, Input } from '@angular/core';
import Post from '../../models/Post';

@Component({
  selector: 'app-feed-post',
  templateUrl: './feed-post.component.html',
  styleUrls: ['./feed-post.component.scss'],
})
export class FeedPostComponent implements OnInit {
  @Input() post: Post;

  constructor() { }

  ngOnInit() {}

}
