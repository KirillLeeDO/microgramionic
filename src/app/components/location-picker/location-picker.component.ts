import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MouseEvent, MapsAPILoader } from '@agm/core';
import AppConfig from 'src/app/app.config';
import Location from 'src/app/models/Location';
import Place from 'src/app/models/Place';

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.scss'],
})
export class LocationPickerComponent implements OnInit {
  private pickedLocation: any;

  public latitude: number;
  public longitude: number;
  public zoom: number;
  public address: string;

  private geoCoder;

  @Input() location: any;

  constructor(
    private modalCtrl: ModalController,
    private mapsAPILoader: MapsAPILoader
  ) { }

  ngOnInit() {
    this.mapsAPILoader.load()
    .then(() => {
      this.geoCoder = new google.maps.Geocoder;
      this.setInitialLocation();
    });
  }

  setInitialLocation() {
    this.latitude = this.location ? this.location.latitude : AppConfig.DEFAULT_LATITUDE;
    this.longitude = this.location ? this.location.longitude : AppConfig.DEFAULT_LONGITUDE;
    this.getAddress(this.latitude, this.longitude);
    this.zoom = 16;
  }

  dismissModal() {
    this.modalCtrl.dismiss();
  }

  markerDragEnd($event: MouseEvent) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }
  
  mapClick($event: MouseEvent) {
    console.log($event);
  }

  getAddress(latitude: number, longitude: number) {
    this.geoCoder.geocode({
      location: {
        lat: latitude,
        lng: longitude
      }
    }, (results, status) => {
      if (status !== 'OK') {
        console.log('Geocoder failed due to ' + status);
        return null;
      }

      const relevantResults = results.filter(result => {
        return (
          result.types.includes("locality") ||
          result.types.includes("administrative_area_level_1")
        );
      });

      if (!relevantResults.length) {
        console.log('NO results');
        return null;
      }

      this.address = relevantResults[0].formatted_address;
    })
  }

  pickLocation() {
    this.modalCtrl.dismiss(new Location(
      this.latitude,
      this.longitude,
      new Place('', this.address)
    ));
  }

}
