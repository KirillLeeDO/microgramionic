import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { PostService } from '../services/post.service';

import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsResolverService implements Resolve<any>{

  constructor(
    private postService: PostService
  ) { }

  resolve() {
    // const dataObservable = this.postService.getPosts();
    const shellProviderObservable = this.postService.getPostsWithShell();

    const observablePromise = new Promise((resolve, reject) => {
      resolve(shellProviderObservable);
    })

    return observablePromise;
  }
}
