import { Injectable } from '@angular/core';
import { FirebaseService } from './firebase.service';
import { AngularFireAuth } from '@angular/fire/auth';
import {} from '@angular/fire';
import UserCredentials from '../models/UserCredentials';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authUser: firebase.User

  constructor(
    private fireService: FirebaseService,
    private afAuth: AngularFireAuth
  ) {
    this.afAuth.user.subscribe((user) => {
      this.authUser = user;
    });
  }

  signUp(credentials: UserCredentials) {
    return this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password)
      .then((res) => {
        return res;
      });
  }

  signIn(credentials: UserCredentials) {
    return this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password)
      .then((res) => {
        return res;
      });
  }

  logout() {
    return this.afAuth.auth.signOut()
      .then((res) => {
        return res;
      });
  }
}
