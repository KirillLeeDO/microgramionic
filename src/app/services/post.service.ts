import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Storage } from '@ionic/storage';
import Post from '../models/Post';
import { File } from '@ionic-native/file/ngx';
import { FirebaseService } from './firebase.service';
import { Observable, of as observableOf } from 'rxjs';
import { map, delay, switchMap } from 'rxjs/operators';
import { FirebaseApp } from '@angular/fire';
import { ShellFeedListModel } from '../shell/shell-feed.model';
import { ShellProvider } from '../shell/shell.provider';


// @TODO: Extract file methods to FileService
@Injectable({
  providedIn: 'root'
})
export class PostService {
  public posts: Observable<any>;
  public postsLocal: any[];

  private isFetched: boolean = false;

  private static readonly POSTS_STORAGE_KEY = 'posts';
  public static readonly FS_REF = 'posts/';
  public static readonly DB_REF = 'posts/';

  private cropOptions: object = {
    quality: 80
  };

  constructor(
    private camera: Camera,
    private file: File,
    private imagePicker: ImagePicker,
    private crop: Crop,
    private webView: WebView,
    private storage: Storage,
    private fireService: FirebaseService
  ) { }

  takePicture() {
    const options: CameraOptions = {
      quality: 80,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    };

    return this.camera.getPicture(options)
      .then((imageData) => {
        return this.crop.crop(imageData, this.cropOptions);
      })
      .then((croppedImage) => {
        return new Post(croppedImage);
        // return new Post(this.convertFileSrc(croppedImage));
      });
  }

  openPicture(currentLocation: any = null) {
    const options = {
      maximumImagesCount: 1
    };

    return this.imagePicker.getPictures(options)
      .then((results) => {
        if (!results.length) {
          throw new Error('No results');
        }

        return this.cropImages(results);
      });
  }

  cropImages(images: any): any {
    return images.reduce((promise: any, item: any) => {
      return promise.then((chainResults) => {
        return this.crop.crop(item, this.cropOptions)
          .then(croppedImage => {
            return [...chainResults, new Post(croppedImage)];
            // return [...chainResults, new Post(this.convertFileSrc(croppedImage))];
          });
      });
    }, Promise.resolve([]));
  }

  // @TODO: blobData => FileBlob model
  async addPost(post: Post, blobData: any = null) {
    try {
      let fileBlob = (blobData === null) ? await this.fileToBlob(post.data) : blobData;
      let uploadedFileURL = await this.fireService.putToStorage(fileBlob, PostService.FS_REF);

      post.data = uploadedFileURL;
      console.log('Post ready to push', post);

      let pushResult = await this.fireService.pushToList(PostService.DB_REF, post);
      console.log('yeaaah', pushResult);
    } catch (e) {
      console.log('addPost error', e);
      throw new Error(e);
    }
  }

  getPost(key: string) {
    return this.fireService.getListItem(PostService.DB_REF, key)
      .pipe(
        map(values => {
          return values.length ? values[0] : {};
        })
      )
  }

  getPosts(): Observable<any> {
    return this.fireService.getListSnapshots(PostService.DB_REF)
      .pipe(
        map(snapshots => {
          return {
            items: snapshots.map((snapshot) => {
              const post = {
                key: snapshot.payload.key,
                ...snapshot.payload.val()
              };
  
              return post;
            })
          };
        })
      )
      .pipe(delay(2000));
  }

  getPostsWithShell(): Observable<ShellFeedListModel> {
    const shellModel: ShellFeedListModel = new ShellFeedListModel(true);
    const dataObservable = this.getPosts();

    const shellProvider = new ShellProvider(
      shellModel,
      dataObservable
    );

    return shellProvider.observable;
  }

  convertFileSrc(src: string): string {
    return this.webView.convertFileSrc(src);
  }

  fileToBlob(imagePath: string) {
    return new Promise((resolve, reject) => {
      let fileName = "";
      this.file
        .resolveLocalFilesystemUrl(imagePath)
        .then((fileEntry) => {
          const { name, nativeURL } = fileEntry;
          const path = nativeURL.substring(0, nativeURL.lastIndexOf("/"));

          // @TODO: Generate hash name?
          fileName = name;

          return this.file.readAsArrayBuffer(path, name);
        })
        .then((buffer) => {
          let imgBlob = new Blob([buffer], { type: "image/jpeg" });

          // @TODO: Create Model FirebaseFileData
          resolve({
            fileName,
            imgBlob
          });
        })
        .catch(e => {
          reject(e);
        });
    });
  }
}