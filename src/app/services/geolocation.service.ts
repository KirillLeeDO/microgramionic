import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import Location from '../models/Location';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { NativeGeocoderOptions, NativeGeocoder, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';
import { HttpClient } from '@angular/common/http';

import Place from '../models/Place';
import AppConfig from '../app.config';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeolocationService {
  private currentLocation: Location = new Location();
  private gmGeocoder;

  private isDesktop: boolean = false;

  private readonly gmApiUrl = 'https://maps.googleapis.com/maps/api/geocode';

  constructor(
    private platform: Platform,
    private geoLocation: Geolocation,
    private geoCoder: NativeGeocoder,
    private httpClient: HttpClient
  ) {
    this.platform.ready()
      .then((source) => {
        if (
          this.platform.is('desktop') ||
          this.platform.is('mobileweb')
        ) {
          this.isDesktop = true;
        }

      });
  }

  private initializeMapsAPI() {
    // this.mapsAPILoader.load()
    //   .then(() => {
    //     this.gmGeocoder = new google.maps.Geocoder;
    //   });

  }

  async getCurrentLocation(): Promise<any> {
    let location = null;

    try {
      location = await (this.isDesktop ? this.getBrowserLocation() : this.getNativeLocaton());
    } catch (err) {
      console.log(err);
    }

    return location;
  }

  private getBrowserLocation() {
    const location = new Location();

    if (!('geolocation' in navigator)) {
      console.log('geolocation is not available');
      return Promise.resolve(null);
    }

    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition((position) => {
        if (!position.coords) {
          resolve(null);
          return;
        }

        let { latitude, longitude } = position.coords;

        location.latitude = latitude;
        location.longitude = longitude;

        this.httpClient.get(`${this.gmApiUrl}/json?latlng=${latitude},${longitude}&key=${environment.gmaps.apiKey}`)
          .toPromise()
          .then((res: any) => {
            const locations = res.results.filter((item) => {
              return item.types.includes('locality') ||
                item.types.includes('administrative_area_level_1');
            });

            if (locations.length) {
              location.place = new Place('', locations[0].formatted_address);
            }

            resolve(location);
          })
          .catch((err) => reject(err));
      });
    });
  }

  private getNativeLocaton() {
    const location = new Location();

    return this.geoLocation.getCurrentPosition()
      .then(({ coords }: Geoposition) => {
        location.latitude = coords.latitude;
        location.longitude = coords.longitude;
        return this.reverseGeocode(coords.latitude, coords.longitude);
      })
      .then((place: Place) => {
        location.place = place;
        return location;
      })
      .catch((error) => {
        console.log('getCurrentLocation error', error);
        return null;
      });
  }

  reverseGeocode(latitude: number, longitude: number) {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 1
    };

    return this.geoCoder.reverseGeocode(latitude, longitude, options)
      .then((results: NativeGeocoderResult[]) => {
        if (results.length < 1) {
          throw new Error('Empty results');
        }

        return new Place(
          results[0].countryName,
          results[0].locality
        );
      });
  }
}
