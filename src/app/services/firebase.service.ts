import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireAction } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize, take } from 'rxjs/operators';
import { database } from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  posts: Observable<any[]>;

  constructor(
    private fireDB: AngularFireDatabase,
    private fireStorage: AngularFireStorage
  ) {
    
  }

  getListSnapshots(listRef: string) {
    return this.fireDB.list(listRef, ref => ref.orderByChild('createdOn')).snapshotChanges();
  }

  getListItem(listRef: string, key: string) {
    return this.fireDB.list(listRef, ref => ref.orderByKey().equalTo(key)).valueChanges();
  }

  pushToList(listRef: string, data) {
    const itemRef = this.fireDB.list(listRef);

    data.createdOn = database.ServerValue.TIMESTAMP;

    return new Promise((resolve, reject) => {
      const promise = itemRef.push(data)
        .then((result) => {
          resolve(result);

          // this.getListItem(listRef, result.key)
          //   .pipe(
          //     take(1)
          //   )
          //   .subscribe((post) => {
          //     console.log('once post', post);
          //     this.fireDB.object(listRef + result.key)
          //       .update({
          //         createdOn: (-1) * 
          //       })
          //   })
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  putToStorage(imageBlobInfo, storageRef: string) {
    return new Promise((resolve, reject) => {
      let fileRef = this.fireStorage.ref(storageRef + imageBlobInfo.fileName);
      let uploadTask = fileRef.put(imageBlobInfo.imgBlob);

      // uploadTask.percentageChanges().subscribe(per => console.log(per));
      uploadTask.snapshotChanges()
        .pipe(
          finalize(() => {
            fileRef.getDownloadURL().subscribe((url) => {
              console.log('url', url);
              resolve(url);
            });
          })
        )
        .subscribe()

      uploadTask.catch((err) => {
        reject(err);
      });
    });
  }
}
