import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PostService } from '../services/post.service';
import Post from '../models/Post';
import { interval } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {
  post: any;
  isLoading: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private service: PostService
  ) { 
    this.post = {};
  }

  ngOnInit() {
    let key = this.route.snapshot.paramMap.get('key');

    this.service.getPost(key)
      .subscribe((post: Post) => {
        this.post = post;
        this.isLoading = false;
      })
  }
}
