import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule, Routes } from '@angular/router';
import { PostComponent } from './post.component';
import { AgmCoreModule } from '@agm/core';
import AppConfig from '../app.config';
import { environment } from 'src/environments/environment';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: PostComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      // apiKey: AppConfig.GOOGLE_MAPS_KEY
      apiKey: environment.gmaps.apiKey
    })
  ],
  declarations: [PostComponent]
})
export class PostModule { }
