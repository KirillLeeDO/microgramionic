import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import UserCredentials from 'src/app/models/UserCredentials';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss', '../register/register.page.scss'],
})
export class SigninPage implements OnInit {
  signInForm: FormGroup;
  isLoading: boolean;

  validationMessages = {
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'pattern', message: 'Email is not valid' },
    ],
    password: [
      { type: 'required', message: 'Password is required' },
      { type: 'minlength', message: 'Password must contain at least 6 chars' },
    ]
  };

  statusMessages = {
    error: '',
    success: ''
  };

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.isLoading = false;
  }

  ngOnInit() {
    this.signInForm = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ]))
    })
  }

  signIn(credentials: UserCredentials) {
    this.statusMessages.success = '';
    this.statusMessages.error = '';

    this.isLoading = true;

    this.authService.signIn(credentials)
      .then((res) => {
        console.log(res);
        this.statusMessages.success = res.user.email;
        this.isLoading = false;
        this.router.navigate(["/tabs/profile"]);
      })
      .catch((err) => {
        console.log(err);
        this.statusMessages.error = err;
        this.isLoading = false;
      });
  }

}
